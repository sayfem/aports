# Contributor: Kevin Daudt <kdaudt@alpinelinux.org>
# Maintainer: Kevin Daudt <kdaudt@alpinelinux.org>
pkgname=opentofu
pkgver=1.6.0_beta3
pkgrel=0
pkgdesc="OpenTofu lets you declaratively manage your cloud infrastructure"
url="https://opentofu.org"
# x86, armhf, armv7: several tests fail
arch="all !x86 !armhf !armv7"
license="MPL-2.0"
makedepends="go"
source="$pkgname-$pkgver.tar.gz::https://github.com/opentofu/opentofu/archive/refs/tags/v${pkgver/_/-}.tar.gz"
builddir="$srcdir/$pkgname-${pkgver/_/-}"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	local ldflags="-X 'github.com/opentofu/opentofu/version.dev=no'"
	go build -ldflags "$ldflags" ./cmd/tofu
}

check() {
	go test -v ./...
}

package() {
	install -Dm0755 tofu -t "$pkgdir"/usr/bin/
}

sha512sums="
40a6f69868c571139102fb2ed29360f14e99cfe98c76cfac5ca8da33656161429cfb068464f84301e3fcee757267e21bb604ba1b78cdf264df60624f788afc73  opentofu-1.6.0_beta3.tar.gz
"
