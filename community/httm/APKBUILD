# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=httm
pkgver=0.32.4
pkgrel=0
pkgdesc="Interactive, file-level Time Machine-like tool for ZFS/btrfs"
url="https://github.com/kimono-koans/httm"
# riscv64: TODO
# s390x: fails to build nix crate
arch="all !riscv64 !s390x"
license="MPL-2.0"
makedepends="cargo acl-dev cargo-auditable"
subpackages="$pkgname-doc"
source="https://github.com/kimono-koans/httm/archive/refs/tags/$pkgver/httm-$pkgver.tar.gz"
options="net !check"  # no tests provided

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release --features acls
}

package() {
	install -D -m755 target/release/$pkgname -t "$pkgdir"/usr/bin/
	install -D -m644 $pkgname.1 -t "$pkgdir"/usr/share/man/man1/
}

sha512sums="
ba2a231120f4a10a71fbb6447ec91782a1c1a1086d64f0a35dca646e7635bbd4eecefcd83fc97fd9c5f9403c7e778bd05dbff5b48f9eebaecc3742de3e88139f  httm-0.32.4.tar.gz
"
