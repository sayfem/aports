# Maintainer: Jeff Dickey <alpine@rtx.pub>
pkgname=rtx
pkgver=2023.12.24
pkgrel=0
pkgdesc="Polyglot runtime and dev tool version manager"
url="https://rtx.pub"
arch="all !s390x !riscv64 !ppc64le" # limited by cargo
license="MIT"
makedepends="cargo bash direnv cargo-auditable openssl-dev"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/jdxcode/rtx/archive/refs/tags/v$pkgver.tar.gz"

prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen --bin rtx
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/rtx -t "$pkgdir/usr/bin/"
	install -Dm644 README.md docs/*.md -t "$pkgdir/usr/share/doc/$pkgname"
	install -Dm644 LICENSE -t "$pkgdir/usr/share/licenses/$pkgname"
	install -Dm644 "man/man1/$pkgname.1" -t "$pkgdir/usr/share/man/man1"
}

sha512sums="
e83ad643dbaf752cd7a6a914a9cad5e737d9c3183db183c74ed5f1cfe8086920a8f0eef1fbaa5fad9f96f5531dc61131a6c3323a5d573c5650f64e9e5bac8726  rtx-2023.12.24.tar.gz
"
